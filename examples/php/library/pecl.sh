# Example usage
# pecl reinstall mongo 1.3.2      # will do ${php_prefix}/bin/pecl uninstall mongo; ${php_prefix}/bin/pecl install mongo-1.3.2
# pecl enable mongo               # will do echo 'extension=mongo.so' > ${php_prefix}/etc/conf.d/mongo.ini


function pecl {
	if [ $1 == reinstall ]
	then
		${php_prefix}/bin/pecl uninstall $2
		${php_prefix}/bin/pecl install $2-$3
	elif [ $1 == enable ]
	then
		echo 'extension=$2.so' > ${php_prefix}/etc/conf.d/$2.ini
	elif [ $1 == uninstall ]
	then
		${php_prefix}/bin/pecl uninstall $2
	elif [ $1 == install ]
	then
		${php_prefix}/bin/pecl install $2-$3
	else
		echo Unknown command
	fi
}
